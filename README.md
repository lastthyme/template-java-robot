This project defines a template structure that can be used to generate new robot projects.

To use:
  1. ```git clone https://gitlab.com/totino-grace-robotics-control-team/template-java-robot.git```
  2. ```git remote remove origin```
  3. ```git remote add origin https://gitlab.com/totino-grace-robotics-control-team/your-new-project.git```
  4. ```git pull --recurse-submodules```
  5. ```git add .```
  6. ```git commit -m "initial commit"```
  7. ```git push -u origin master```
  