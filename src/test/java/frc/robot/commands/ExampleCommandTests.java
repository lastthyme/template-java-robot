package frc.robot.commands;

import org.junit.Test;

import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.helpers.SchedulerHelper;
import frc.robot.subsystems.ExampleSubsystem;
import frc.robot.utils.Debug;

public class ExampleCommandTests {

    @Test
    public void test() {
        Debug.TerminalColors.chosenColors.clear();
        SchedulerHelper.schedulerStart();

        ExampleSubsystem es = new ExampleSubsystem();
        ExampleCommand ec = new ExampleCommand(es);

        ec.schedule();
        CommandScheduler.getInstance().run();
    }
}
