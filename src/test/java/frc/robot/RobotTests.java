package frc.robot;

import static org.junit.Assert.assertEquals;

import frc.robot.helpers.PrintStreamTestHelper;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RobotTests {

    // Store a mock
    private Robot robotMock;

    @Before
    public void init() {
        robotMock = new Robot();
    }

    // Rigorous test
    @Test
    public void robotSaysHello_bare() {
        // Have this as a way to undo the binding
        final PrintStream originalOut = System.out;

        // Setup a new print stream
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));

        // Start the robot
        robotMock.robotInit();

        // Assertion
        assertEquals("Hello, World!\n", bos.toString());

        // Undo the binding in System
        System.setOut(originalOut);
    }

    @Test
    public void robotSaysHello_Helpers() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final PrintStream ps = PrintStreamTestHelper.setupMockPrintStream(baos);

        robotMock.robotInit();

        assertEquals("Hello, World!\n", baos.toString());

        PrintStreamTestHelper.undoBinding(ps);
    }

    @Test
    public void robotSaysHello_HelperWrapper() {
        PrintStreamTestHelper.testSystemPrint("Hello, World!\n", () -> robotMock.robotInit());
    }

    @After
    public void done() {
        robotMock.close();
    }
}
