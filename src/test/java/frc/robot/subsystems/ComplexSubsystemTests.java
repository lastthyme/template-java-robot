package frc.robot.subsystems;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Spark;
import frc.robot.helpers.MockHardwareExtension;
import frc.robot.helpers.SchedulerHelper;

import org.junit.Before;
import org.junit.Test;

public class ComplexSubsystemTests {

    // Assemble using Mockito to mock our dependencies. We don't want to have to
    // wire up real hardware to test the subsystem
    Spark motorMock = null;
    DoubleSolenoid pistonMock = null;
    DigitalInput ballSensorMock = null;
    ComplexSubsystem complexSubsystem = null;

    /**
     * Create the mocks here and initialize all the HAL components.
     */
    @Before
    public void preTests() {
        // Create mocks
        motorMock = mock(Spark.class);
        pistonMock = mock(DoubleSolenoid.class);
        ballSensorMock = mock(DigitalInput.class);

        // Wire up our subsystem
        complexSubsystem = new ComplexSubsystem(motorMock, pistonMock, ballSensorMock);

        // Setup HAL + hardware
        SchedulerHelper.schedulerStart();
        SchedulerHelper.schedulerClear();
        MockHardwareExtension.initializeHardware();
    }

    @Test
    @SuppressWarnings("checkstyle:MagicNumber")
    public void shouldMovePistonWhenEjectingBall() throws InterruptedException {
        // Act
        complexSubsystem.ejectBall();

        // Assert
        verify(pistonMock, times(1)).set(DoubleSolenoid.Value.kForward);

        // Run the scheduler
        SchedulerHelper.runForDuration(complexSubsystem, 250, 200);

        // Assert
        verify(pistonMock, times(1)).set(DoubleSolenoid.Value.kReverse);
    }
}
