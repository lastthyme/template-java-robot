package frc.robot.subsystems;

import static org.mockito.Mockito.*;

import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.commands.ExampleCommand;
import frc.robot.helpers.MockButton;
import frc.robot.helpers.MockHardwareExtension;
import frc.robot.helpers.PrintStreamTestHelper;
import frc.robot.helpers.SchedulerHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This is an example test of the robot. This is to make sure that everything is
 * working as intended before code goes on a robot.
 */
public class ExampleSubsystemTests {

    // Mock instance of Example Subsystem
    private ExampleSubsystem exampleSubsystem;
    private ExampleSubsystem mockedExampleSubsystem;

    /**
     * This method is run before the tests begin. Initialize all mocks you wish to
     * use in multiple functions here. Copy and paste this function in your own
     * test.
     */
    @Before
    public void before() {
        SchedulerHelper.schedulerStart();
        SchedulerHelper.schedulerClear();
        MockHardwareExtension.initializeHardware();

        mockedExampleSubsystem = mock(ExampleSubsystem.class);
        exampleSubsystem = new ExampleSubsystem();
    }

    /**
     * This test makes sure that the subsystemMethod of the example subsystem does
     * the correct things.
     */
    @Test
    public void exampleSubsystemMethodSaysHello() {
        PrintStreamTestHelper.testSystemPrint("Hello, World!\n", () -> exampleSubsystem.sayHello());
    }

    /**
     * This test makes sure that the example command calls the subsystemMethod of
     * example subsystem is called exactly once.
     */
    @Test
    public void exampleCommandOnExampleSubsystem() {
        // Create command
        ExampleCommand exampleCommand = new ExampleCommand(mockedExampleSubsystem);

        // Create a fake button that will be "pressed"
        MockButton fakeButton = new MockButton();

        // Tell the button to run example command when pressed
        fakeButton.whenPressed(exampleCommand);

        // Push the button and run the scheduler once
        fakeButton.push();
        CommandScheduler.getInstance().run();
        fakeButton.release();

        // Verify that subsystemMethod was called once
        verify(mockedExampleSubsystem, times(1)).sayHello();

        // Clear the scheduler
        SchedulerHelper.schedulerClear();
    }

    /**
     * This test makes sure that periodic is called properly (odd case as this
     * should already work, but you may want to test methods inside of periodic).
     */
    @Test
    public void exampleSubsystemCallsPeriodic() {
        // Make sure that the scheduler has the subsystem registered
        CommandScheduler.getInstance().registerSubsystem(mockedExampleSubsystem);

        // run the scheduler once
        CommandScheduler.getInstance().run();

        // Verify that periodic was called once
        verify(mockedExampleSubsystem, times(1)).periodic();

        // Clear the scheduler
        SchedulerHelper.schedulerClear();
    }

    /**
     * This is called after tests, and makes sure that nothing is left open and
     * everything is ready for the next test class.
     */
    @After
    public void after() {
        SchedulerHelper.schedulerDestroy();
        MockHardwareExtension.releaseAll();
    }
}
