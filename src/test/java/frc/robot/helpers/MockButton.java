package frc.robot.helpers;

import edu.wpi.first.wpilibj2.command.button.Button;

/**
 * Simple mock button class.
 */
public class MockButton extends Button {
    private boolean pushed = false;

    @Override
    public boolean get() {
        return pushed;
    }

    public void push() {
        pushed = true;
    }

    public void release() {
        pushed = false;
    }
}
