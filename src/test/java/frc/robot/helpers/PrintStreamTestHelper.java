package frc.robot.helpers;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public final class PrintStreamTestHelper {

    /**
     * Configures a mock print stream to take over the original System.out print
     * stream.
     *
     * @param baos the byte array output stream
     * @return the original System.out print stream
     */
    public static PrintStream setupMockPrintStream(ByteArrayOutputStream baos) {
        // Have this as a way to undo the binding
        final PrintStream originalOut = System.out;

        // Setup a new print stream
        System.setOut(new PrintStream(baos));

        // Return the original print stream
        return originalOut;
    }

    /**
     * Reverses the effects of {@link #setupMockPrintStream()}.
     *
     * @param originalOut - the original System print stream
     */
    public static void undoBinding(PrintStream originalOut) {
        // Undo the binding in System
        System.setOut(originalOut);
    }

    /**
     * Wrapper method to perform {@link #setupMockPrintStream()},
     * {@link #getPrintStreamOutput()}, {@link #undoBinding(PrintStream)} all in one
     * method.
     *
     * @param expectedResult the expected result to compare against
     * @param function       the method to execute
     */
    public static void testSystemPrint(final String expectedResult, Runnable function) {
        // Have this as a way to undo the binding
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final PrintStream originalOut = PrintStreamTestHelper.setupMockPrintStream(baos);

        // Run the function
        function.run();

        // Test the output
        assertEquals(expectedResult, baos.toString());

        // Undo the binding
        PrintStreamTestHelper.undoBinding(originalOut);
    }
}
