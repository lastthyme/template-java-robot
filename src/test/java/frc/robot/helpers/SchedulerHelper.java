package frc.robot.helpers;

import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import java.util.concurrent.TimeUnit;

/**
 * Integration test helper class that will run the command scheduler. Use this
 * class when your test requires commands or command groups to be exercised with
 * the full WPI scheduler.
 */
public final class SchedulerHelper {
    @SuppressWarnings("checkstyle:MagicNumber")
    private static int defaultHeartbeatInMs = 20;

    /**
     * Static class. Do not initialize.
     */
    private SchedulerHelper() {
    }

    /**
     * Change the default heartbeat for the scheduler pump.
     *
     * @param defaultHeartbeatInMs Heartbeat in milliseconds
     */
    public static void setDefaultHeartbeat(int defaultHeartbeatInMs) {
        SchedulerHelper.defaultHeartbeatInMs = defaultHeartbeatInMs;
    }

    /**
     * Helper to figure out what heartbeat to use.
     *
     * @param optionalHeartbeatInMs Optional heartbeat in array form to simulate
     *                              optional parameters
     * @return The heartbeat to use
     */
    private static int getHeartbeatToUse(int[] optionalHeartbeatInMs) {
        if (optionalHeartbeatInMs.length > 1) {
            throw new IllegalArgumentException("There can be only one optional heartbeat parameter.");
        }

        return optionalHeartbeatInMs.length > 0 ? optionalHeartbeatInMs[0] : defaultHeartbeatInMs;
    }

    /**
     * Run the command scheduler every heartbeatInMs for a durationInMs amount of
     * time. Calls will be serialized as the Scheduler is not thread safe, so beware
     * of deadlocks. As of this writing, parallel testing is NOT the default mode
     * for JUnit. So if you have not decorated your tests to run in parallel, you
     * are fine.
     *
     * @param subsystem             the subsystem to register
     * @param durationInMs          Duration to run in milliseconds
     * @param optionalHeartbeatInMs Optional pump time in milliseconds. If omitted,
     *                              20ms default unless changed.
     * @throws InterruptedException Thrown if sleeping interrupted
     */
    public static synchronized void runForDuration(SubsystemBase subsystem, long durationInMs,
            int... optionalHeartbeatInMs) throws InterruptedException {
        // Register the subsystem
        CommandScheduler.getInstance().registerSubsystem(subsystem);

        // Retrieve the heartbeat value
        int heartbeatToUseInMs = getHeartbeatToUse(optionalHeartbeatInMs);

        // Record the start time
        long start = System.nanoTime();

        // Execute the scheduler based on heartbeat period for desired amount of time
        while (System.nanoTime() < (start + TimeUnit.MILLISECONDS.toNanos(durationInMs))) {
            CommandScheduler.getInstance().run();
            Thread.sleep(heartbeatToUseInMs);
        }

        // Unregister the subsystem
        CommandScheduler.getInstance().unregisterSubsystem(subsystem);
    }

    /**
     * Start the scheduler.
     */
    public static void schedulerStart() {
        CommandScheduler.getInstance().enable();
    }

    /**
     * Clear the scheduler.
     */
    public static void schedulerClear() {
        CommandScheduler.getInstance().cancelAll();
        CommandScheduler.getInstance().clearButtons();
    }

    /**
     * Destroy the scheduler.
     */
    public static void schedulerDestroy() {
        CommandScheduler.getInstance().disable();
    }
}
