package frc.robot.utils;

import org.junit.Before;
import org.junit.Test;

public class DebuggerTests {

    // Mock instance of the debugger
    private Debug m_debugger;

    @Before
    public void before() {
        m_debugger = new Debug("Test suite");
    }

    @Test
    public void debuggerSaysHello() {
        m_debugger.setEnabled(true);
        m_debugger.requestTerminalColor(Debug.TerminalColors.PURPLE);
        m_debugger.log("Hello, World!");
        m_debugger.requestTerminalColor(Debug.TerminalColors.BLUE);
        m_debugger.log("Hello, World!2");
    }
}
