/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {

    public static final class ComplexSubsystemConstants {
        public static final int kMotorPort_PWM = 2;
        public static final int kPistonOutPort_DIO = 4;
        public static final int kPistonInPort_DIO = 5;
        public static final int kDigitalInputPort_DIO = 6;
    }

    public static final class DriveTrainConstants {
        public static final int kLeftMotorPort_PWM = 0;
        public static final int kRightMotorPort_PWM = 1;

        public static final int[] kLeftDriveEncoder_DIO = new int[] { 0, 1 };
        public static final int[] kRightDriveEncoder_DIO = new int[] { 2, 3 };
    }

    public static final class OiConstants {
        public static final int kDriveControllerPort_USB = 0;
        public static final int kWeaponsControllerPort_USB = 1;
    }
}
