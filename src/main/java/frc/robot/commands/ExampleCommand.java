/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExampleSubsystem;
import frc.robot.utils.Debug;

/**
 * An example command that uses the example subsystem. When this example command
 * is scheduled, using the {@link #CommandScheduler}, the {@link #initialize()}
 * method will be run once then the {@link #execute()} method will be ran
 * continuously until the {@link #isFinished()} method returns true at which
 * point the {@link #end(boolean)} method will be called so you can clean up if
 * needed.
 */
public class ExampleCommand extends CommandBase {

    // Create a debugger for this command, to log helpful messages here and there
    Debug m_debugger = new Debug("Example command").setEnabled(true);

    // Store the subsystem that is going to be used by this command
    private final ExampleSubsystem m_subsystem;

    /**
     * Creates a new ExampleCommand.
     *
     * @param subsystem The subsystem used by this command.
     */
    public ExampleCommand(ExampleSubsystem subsystem) {
        m_debugger.log("constructor was called");

        // Use addRequirements() here to declare subsystem dependencies.
        m_subsystem = subsystem;
        addRequirements(subsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        // Nothing to do here but log some example messages...
        m_debugger.log("initialize method");
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        m_debugger.log("execute method");
        m_subsystem.sayHello();
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        // Nothing to do here but log some example messages...
        m_debugger.log("end method | was interrupted: " + interrupted);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        m_debugger.log("isFinished method");
        return true;
    }
}
