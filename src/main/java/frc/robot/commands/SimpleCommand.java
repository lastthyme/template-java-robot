package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.ExampleSubsystem;

/**
 * A simple instant command. This command will end instantly after calling the
 * {@link #initialize()} method
 */
public class SimpleCommand extends InstantCommand {

    // Store the subsystem that is going to be used by this command
    private final ExampleSubsystem m_subsystem;

    /**
     * Creates a new ExampleCommand.
     *
     * @param subsystem The subsystem used by this command.
     */
    public SimpleCommand(ExampleSubsystem subsystem) {
        m_subsystem = subsystem;

        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(subsystem);
    }

    // Say hello when this command is run
    @Override
    public void initialize() {
        m_subsystem.sayHello();
    }
}
