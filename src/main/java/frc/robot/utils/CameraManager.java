package frc.robot.utils;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoSink;
import edu.wpi.cscore.VideoSource;
import edu.wpi.cscore.VideoSource.ConnectionStrategy;
import edu.wpi.first.cameraserver.CameraServer;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple camera manager class, hopefully will see improvement later in the
 * season. This class is a singleton, use it as such!
 */
public final class CameraManager {

    // Camera locations
    public static enum CameraLocations {
        FRONT_BALL_PICKUP, BACK_SHOOTER, TELESCOPE_HOOK, NONE;

        // Get the next camera on the list
        public CameraLocations next() {
            return values()[(this.ordinal() + 1) % (values().length - 1)];
        }
    }

    // Default camera config
    public static final UsbCamera[] kDefaultCameraConfig = new UsbCamera[] {
            CameraServer.getInstance().startAutomaticCapture(0), CameraServer.getInstance().startAutomaticCapture(1),
            CameraServer.getInstance().startAutomaticCapture(2), };

    // Object the camera server
    private VideoSink m_vidSink;

    // What camera is currently streaming data
    private CameraLocations m_streamingCamera = CameraLocations.NONE;
    private final Map<CameraLocations, VideoSource> m_cameraMap = new HashMap<CameraLocations, VideoSource>(0);

    // Camera Constants
    private final int k_CameraFps = 20;
    private final int[] k_CameraResolution = new int[] { 320, 240 };

    // Singleton instance reference
    private static CameraManager m_cameraManagerInstance = null;

    /**
     * Creates a new CameraManager. Dependency injection to make it more testable
     */
    private CameraManager(final VideoSource[] systemCameras) throws IllegalArgumentException {
        // If there are more cameras than locations
        if (systemCameras.length >= CameraLocations.values().length) {
            throw new IllegalArgumentException("There were more system cameras provided than locations");
        }

        // Setup all the system cameras
        for (int i = 0; i < systemCameras.length; i++) {
            m_cameraMap.put(CameraLocations.values()[i], systemCameras[i]);
        }

        // Grab the camera sink server only if there are sources to grab from
        m_vidSink = CameraServer.getInstance().getServer();

        // Initialize all the cameras
        for (VideoSource c : m_cameraMap.values()) {
            c.setConnectionStrategy(ConnectionStrategy.kKeepOpen);
            c.setFPS(k_CameraFps);
        }

        // Set the first camera on instance creation
        if ((m_streamingCamera == CameraLocations.NONE) && (systemCameras.length > 0)) {
            toggleCamera(CameraLocations.FRONT_BALL_PICKUP);
        }
    }

    /**
     * Call this from a command to switch camera feeds.
     *
     * @param cameraIndex the camera to start streaming
     * @throws IllegalArgumentException if there is no camera connected to the
     *                                  system with the specified index
     */
    @SuppressWarnings("checkstyle:LineLength")
    public void toggleCamera(final CameraLocations cameraIndex) throws IllegalArgumentException {
        // Check the param
        if (m_cameraMap.get(cameraIndex) == null) {
            throw new IllegalArgumentException("There is no camera connected to the system with name: " + cameraIndex);
        }

        // Placeholder for requested camera
        final UsbCamera requestedCamera = (UsbCamera) m_cameraMap.get(cameraIndex);

        // Switch to the requested camera
        initCamera(requestedCamera);
        m_vidSink.setSource(requestedCamera);

        // Turn off all other cameras
        m_cameraMap.entrySet().stream().filter(entry -> entry.getKey() != cameraIndex).map(entry -> entry.getValue())
                .filter(entry -> entry.getKind() == VideoSource.Kind.kUsb).forEach(this::shutdownCamera);
    }

    /**
     * Call this from a command to switch camera feeds. Toggles to the next safe
     * camera/stream on the list
     */
    public void toggleCamera() {
        // Since this method will most likely be called from a command during
        // competition time, it is best to make sure that no exceptions will be thrown
        CameraLocations nextLoc = m_streamingCamera.next();

        // Get the next safe camera location
        while (m_cameraMap.get(nextLoc) == null) {
            nextLoc = nextLoc.next();
        }

        // Start streaming the next safe camera
        toggleCamera(nextLoc);
    }

    /**
     * Saves bandwidth when the camera is not streaming.
     *
     * @param camera the camera to shutdown
     */
    public void shutdownCamera(VideoSource camera) {
        camera.setResolution(0, 0);
    }

    /**
     * Initializes a camera from shutdown.
     *
     * @param camera the camera to initialize
     */
    public void initCamera(VideoSource camera) {
        // Pull camera configuration from constants
        final int width = k_CameraResolution[0];
        final int height = k_CameraResolution[1];

        // Set camera values
        camera.setResolution(width, height);
    }

    /**
     * Adds a camera to the system.
     *
     * @param cameraLocation where the camera is located
     * @param newCamera      the camera/stream object
     * @param forceReplace   if there is a camera/stream already in this location,
     *                       this will force replace it with the new camera/stream
     */
    public void addCamera(CameraLocations cameraLocation, VideoSource newCamera, boolean forceReplace) {
        // Get the old camera, if there is one
        VideoSource oldCamera = m_cameraMap.get(cameraLocation);

        // If there is a camera/stream already in this location, and not forcing then do
        // not remove the old camera
        if ((oldCamera != null) && (!forceReplace)) {
            return;
        }

        // Put the new camera in the place
        m_cameraMap.put(cameraLocation, newCamera);

        // If it is a usb camera, shut it down so it does not consume bandwidth but keep
        // the connection open
        if (newCamera.getKind() == VideoSource.Kind.kUsb) {
            // Cast the type
            newCamera = (UsbCamera) newCamera;

            // Set options
            newCamera.setConnectionStrategy(ConnectionStrategy.kKeepOpen);
            newCamera.setFPS(k_CameraFps);
            shutdownCamera(newCamera);
        }
    }

    /**
     * Get the current camera streaming.
     *
     * @return the streaming camera location
     */
    public CameraLocations getCameraStreamingToDashboard() {
        return m_streamingCamera;
    }

    /**
     * Retrieves the current CameraManager instance.
     *
     * @param cams the camera sources
     */
    public static CameraManager getInstance(final VideoSource[] cams) {
        // If an instance has not been made yet, create one
        if (m_cameraManagerInstance == null) {
            m_cameraManagerInstance = new CameraManager(cams);
        }

        // Return the current instance
        return m_cameraManagerInstance;
    }

    /**
     * Retrieves the current CameraManager instance. If no CameraManager instance
     * exists yet, one will be created using the provided system cameras.
     */
    public static CameraManager getInstance() {
        // If an instance has not been made yet, create one
        if (m_cameraManagerInstance == null) {
            return getInstance(kDefaultCameraConfig);
        }

        // Return the current instance
        return m_cameraManagerInstance;
    }
}
