package frc.robot.utils;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.WaitCommand;

import java.util.concurrent.TimeUnit;

/**
 * This class is a simple wrapper for some of the commonly used methods related
 * to time. All methods in this class are static and thus do not require an
 * instance object to access them.
 *
 * <p>
 * Use {@link #getMatchSeconds()} to get the match seconds
 *
 * <p>
 * Use {@link #getMatchMinutes()} to get the match minutes
 *
 * <p>
 * Use {@link #getGamePeriod()} to get the game period
 *
 * <p>
 * Use
 * {@link #executeAfterDelay(double, TimeUnit, Runnable, SubsystemBase, boolean)}
 * to execute a certain action after a delay. This method is non blocking and
 * asynchronous!
 */
public final class Clock {

    // Game state enum
    public static enum GamePeriods {
        None, Autonomous, Teleoperated, Endgame
    }

    @SuppressWarnings("checkstyle:MagicNumber")
    public static int getMatchSeconds() {
        return (int) Math.floor(Timer.getMatchTime() % 60);
    }

    @SuppressWarnings("checkstyle:MagicNumber")
    public static int getMatchMinutes() {
        return (int) Timer.getMatchTime() / 60;
    }

    /**
     * Determines what period of the game we are currently in.
     *
     * @return the period
     */
    @SuppressWarnings("checkstyle:RightCurly")
    public static GamePeriods getGamePeriod() {
        final int kAutoBeginsMatchSeconds = 0;
        final int kTeleopBeginsMatchSeconds = 35;
        final int kEndgameBeginsMatchSeconds = 150;

        // At 2 min 30 secs we are in endgame
        if (Timer.getMatchTime() >= kEndgameBeginsMatchSeconds) {
            return GamePeriods.Endgame;
        }

        // At 35 secs in we are in teleop
        else if (Timer.getMatchTime() >= kTeleopBeginsMatchSeconds) {
            return GamePeriods.Teleoperated;
        }

        // At 0 secs in we are in autonomous
        else if (Timer.getMatchTime() >= kAutoBeginsMatchSeconds) {
            return GamePeriods.Autonomous;
        }

        // Otherwise we are not in a any state
        return GamePeriods.None;
    }

    /**
     * Schedules a certain action to take place sometime in the future. This method
     * is non blocking and asynchronous!
     *
     * @param seconds       how far into the future this action should take place
     * @param action        the action to execute
     * @param requirement   what subsystem this action requires, if any
     * @param interruptible wether or not this action can be interrupted
     *
     * @throws IllegalArgumentException time can not be negative
     */
    public static void executeAfterDelay(double seconds, Runnable action, SubsystemBase requirement,
            boolean interruptible) throws IllegalArgumentException {
        // Check time param
        if (seconds < 0) {
            throw new IllegalArgumentException("Time to wait can not be negative");
        }

        // Create a non blocking wait command
        WaitCommand nonBlockingWait = new WaitCommand(seconds);

        // Transform the java runnable to a frc command
        InstantCommand runAfterDelay;
        if (requirement != null) {
            runAfterDelay = new InstantCommand(action, requirement);
        } else {
            runAfterDelay = new InstantCommand(action);
        }

        // Add the wait and run commands together to make a sequential command
        SequentialCommandGroup scg = new SequentialCommandGroup(nonBlockingWait, runAfterDelay);
        if (requirement != null) {
            scg.addRequirements(requirement);
        }

        // Schedule the sequential command and make it interruptible
        CommandScheduler.getInstance().schedule(interruptible, scg);
    }

    /**
     * Schedules a certain action to take place sometime in the future. This method
     * is non blocking and asynchronous!
     *
     * @param time          how far into the future this action should take place
     * @param tu            the unit of the input time
     * @param action        the action to execute
     * @param requirement   what subsystem this action requires, if any
     * @param interruptible wether or not this action can be interrupted
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    public static void executeAfterDelay(double time, TimeUnit tu, Runnable action, SubsystemBase requirement,
            boolean interruptible) {
        // Convert the input timeUnit to seconds (with a decimal point!)
        double timeUnitInSeconds = (double) ((double) tu.toMillis((long) time) / (double) 1000);

        // Call the method that takes seconds
        executeAfterDelay(timeUnitInSeconds, action, requirement, interruptible);
    }
}
