package frc.robot.utils;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * This class should help with debugging, at least I hope.
 */
public final class Debug {

    // Keep track of who this debugger is for, the messenger
    private final String m_messenger;

    // Should we be printing logs from this messenger
    private boolean m_enabled = false;

    // Keeps track of previous values to tell when they change
    private final Map<String, Object> m_prevValues;

    // Enum for managing terminal colors
    public static enum TerminalColors {
        RESET("\033[0m"), RED("\033[0;31m"), GREEN("\033[0;32m"), YELLOW("\033[0;33m"), BLUE("\033[0;34m"),
        PURPLE("\033[0;35m"), CYAN("\033[0;36m"), WHITE("\033[0;37m");

        private final String code;
        public static List<Integer> chosenColors = new ArrayList<Integer>(0);

        TerminalColors(String code) {
            this.code = code;
        }

        @Override
        public String toString() {
            return code;
        }
    }

    // What color should this debugger be in the console
    private TerminalColors m_debuggerColor = null;

    /**
     * Creates a new Debugging instance.
     *
     * @param messenger the name of the messenger to show in the console
     */
    public Debug(String messenger) {
        m_messenger = messenger;
        m_prevValues = new HashMap<String, Object>(0);
        System.out.println("Debugging logger created for: " + m_messenger + ", enabled: " + m_enabled);

        clearPrevValues();
        setTerminalColor(messenger);
    }

    /**
     * Logs a message to the console, formats it with the message sender in front.
     *
     * @param message the message to log
     */
    public void log(String message) {
        String formattedMessage = m_messenger + " - " + message;

        if (m_enabled) {
            System.out.println(m_debuggerColor + formattedMessage + TerminalColors.RESET);
        }
    }

    /**
     * Logs a message to the console only when it changes from the last log.
     *
     * @param name  the name of the object you are tracking
     * @param value the current value of the object
     */
    public void logOnChange(String name, Object value) {
        // Error checking
        if (name == "" || name == " " || name == null) {
            return;
        }
        if (m_prevValues.get(name) == null) {
            m_prevValues.put(name, 0);
        }

        // Log when the value has changed
        if (value != m_prevValues.get(name)) {
            log(name + " has updated to: " + value);
        }

        // Update the value
        m_prevValues.put(name, value);
    }

    /**
     * Logs a message to the console only once every second.
     *
     * @param name  the name of the object you are tracking
     * @param value the current value of the object
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    public void logEverySecond(String name, Object value) {
        // Error checking
        if (name == "" || name == " " || name == null) {
            return;
        }
        if (m_prevValues.get(name) == null) {
            m_prevValues.put(name, System.currentTimeMillis());
        }

        // Log the value if its been one second
        if (System.currentTimeMillis() - (long) m_prevValues.get(name) >= 1000) {
            log(name + ": " + value);
            m_prevValues.put(name, null);
        }
    }

    /**
     * Set the status of the debugger.
     *
     * @param enabled if the debugger should be enabled
     */
    public Debug setEnabled(boolean enabled) {
        m_enabled = enabled;

        // Log something
        String output = enabled ? "(Output logging has been enabled)" : "(Output logging has been disabled)";
        log(output);

        return this;
    }

    /**
     * Checks if the debugger is enabled.
     */
    public boolean isEnabled() {
        return m_enabled;
    }

    /**
     * Clears all previous stored values for the
     * {@link #logOnChange(String, Object)} method.
     */
    public void clearPrevValues() {
        m_prevValues.clear();
    }

    /**
     * Sets the current terminal color.
     *
     * @param messenger the name of the messenger
     */
    private void setTerminalColor(String messenger) {
        // Hash the messenger name into a SHA-256 byte array
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Could not initialize debugger for: " + m_messenger);
            e.printStackTrace();
            return;
        }
        byte[] hash = md.digest(messenger.getBytes(StandardCharsets.UTF_8));

        // Convert the byte array to a long
        ByteBuffer wrapped = ByteBuffer.wrap(hash);
        long seed = wrapped.getLong();

        // Seed a random number generator with the long
        Random rng = new Random(seed);

        // If all the colors have been through once, then reset them
        if (TerminalColors.chosenColors.size() == TerminalColors.values().length - 1) {
            TerminalColors.chosenColors.clear();
        }

        // Get a new random color that has not been chosen
        int index = rng.nextInt(TerminalColors.values().length - 1) + 1;
        while (TerminalColors.chosenColors.contains(index)) {
            index = rng.nextInt(TerminalColors.values().length - 1) + 1;
        }

        // Add it to the chosen colors
        TerminalColors.chosenColors.add(index);

        // Set the value
        TerminalColors tc = TerminalColors.values()[index];
        m_debuggerColor = tc;
    }

    /**
     * Sets the debug color to the specified color.
     *
     * @param color the desired color
     */
    public void requestTerminalColor(TerminalColors color) {
        m_debuggerColor = color;
    }

    public void newRandomTerminalColor() {
        setTerminalColor("");
    }
}
