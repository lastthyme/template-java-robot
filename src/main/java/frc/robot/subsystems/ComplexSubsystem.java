/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.Clock;
import frc.robot.utils.Debug;

/**
 * This example complex subsystem handles shooting balls using a motor, reading
 * a limit switch, and ejecting balls using a solenoid piston.
 */
public class ComplexSubsystem extends SubsystemBase {

    // Create a debugger for this subsystem, to log helpful messages here and there
    Debug m_debugger = new Debug("Example Complex Subsystem").setEnabled(true);

    // Declare subsystem members here, but do not construct them yet
    private Spark m_motorController;
    private DigitalInput m_limitSwitch;
    private DoubleSolenoid m_piston;

    /**
     * External dependencies should be "injected" into this class to make it more
     * testable. You can then create a static {@link #factory()} on this class if
     * you like so that you do not have to do the new dependent object creation
     * ceremony every time you want an instance of the ComplexSubsystem.
     *
     * @param motor    - the motor controller for this subsystem
     * @param solenoid - the solenoid for this subsystem
     * @param input    - the digital input device for this subsystem
     */
    public ComplexSubsystem(Spark motor, DoubleSolenoid solenoid, DigitalInput input) {
        this.m_motorController = motor;
        this.m_limitSwitch = input;
        this.m_piston = solenoid;
    }

    /**
     * ComplexSubsystem Factory.
     *
     * @return ComplexSubsystem
     */
    public static ComplexSubsystem factory() {
        // Setup dependencies to inject
        Spark motor = new Spark(Constants.ComplexSubsystemConstants.kMotorPort_PWM);
        DoubleSolenoid solenoid = new DoubleSolenoid(Constants.ComplexSubsystemConstants.kPistonOutPort_DIO,
                Constants.ComplexSubsystemConstants.kPistonInPort_DIO);
        DigitalInput ballSensor = new DigitalInput(Constants.ComplexSubsystemConstants.kDigitalInputPort_DIO);

        // Make and return a new instance
        return new ComplexSubsystem(motor, solenoid, ballSensor);
    }

    // This method will be called once per scheduler run
    @Override
    public void periodic() {
        // Create a message tag and body
        final String messageTag = "periodic-method";
        final String messageBody = "periodic method has been called";

        // Log the message only once every second
        m_debugger.logEverySecond(messageTag, messageBody);
    }

    /**
     * While you could split this method into two separate methods, something like
     * extend piston and retract piston, it doesn't make as much sense to do that.
     * From the outside world, we don't really care about extending or retracting
     * the piston, what we really want to do it eject a ball or something similar.
     */
    public void ejectBall() {
        // Set the delay time here
        final double kDelayTime = 0.200;

        // Extend the piston
        m_piston.set(DoubleSolenoid.Value.kForward);

        // Retract the piston after 200 milliseconds
        Clock.executeAfterDelay(kDelayTime, () -> m_piston.set(DoubleSolenoid.Value.kReverse), this, false);
    }

    /**
     * Sets the motor to shooting speed.
     */
    public void shootBall() {
        final int kShooterSpeed = 1;
        m_motorController.set(kShooterSpeed);
    }

    /**
     * Stops the shooter.
     */
    public void stopShooter() {
        m_motorController.set(0);
    }

    /**
     * Gets the state of the limit switch.
     */
    public boolean getLimitSwitchState() {
        return m_limitSwitch.get();
    }
}
