import re
import plyj.parser
import plyj.model as m

p = plyj.parser.Parser()
f = open("./port map.txt", "w")
tree = p.parse_file('./src/main/java/frc/robot/Constants.java')
dios = []
pwms = []


def log(message):
    print(message)
    f.write(message + "\n")

def printDelcaredTypes():
    print('declared types:')
    for type_decl in tree.type_declarations:
        print(type_decl.name)
        if type_decl.extends is not None:
            print(' -> extending ' + type_decl.extends.name.value)
        if len(type_decl.implements) is not 0:
            print(' -> implementing ' + ', '.join([type.name.value for type in type_decl.implements]))
        print


def checkVariable(type_name, var_name, var_value):
    log('\t' + type_name + ' ' + var_name + ' -> ' + str(var_value))
    if re.search("(_PWM)+", var_name) is not None:
        pwms.append(var_value)
    if re.search("(_DIO)+", var_name) is not None:
        dios.append(var_value)


def checkVariableType(var_decl, type_name):
    if type(var_decl.initializer) is m.Literal:
        checkVariable(type_name, var_decl.variable.name, var_decl.initializer.value)
    elif type(var_decl.initializer) is m.ArrayCreation:
        for elm in var_decl.initializer.initializer.elements:
            checkVariable(type_name, var_decl.variable.name, elm.value)


def evaluateVariableDeclaration(field_decl):
    for var_decl in field_decl.variable_declarators:
        if type(field_decl.type) is str:
            type_name = field_decl.type
        else:
            type_name = field_decl.type.name
    checkVariableType(var_decl, type_name)

def evaluateFieldDeclarations(type_decl):
    for class_decl in [decl for decl in type_decl.body if type(decl) is m.ClassDeclaration]:
        print(class_decl.name)
        f.write(class_decl.name + "\n")
        for field_decl in [decl for decl in class_decl.body if type(decl) is m.FieldDeclaration]:
            evaluateVariableDeclaration(field_decl)


def printPins(arr):
    for elm in arr:
        f.write("    " + elm + "\n")

def evaluateForOverlaps(arr, name):
    if len(set(arr)) is not len(arr):
        print("\033[91m*** DUPLICATE " + name + " PORT IN USE, INSPECT THE CONSTANTS.JAVA FILE! ***\033[0m")
        dupes = set([x for x in arr if arr.count(x) > 1])
        print("\033[91m*** " + name + " PORTS IN CONFLICT: " + ', '.join(list(dupes)) + " ***\033[0m")
        f.write("\n*** DUPLICATE " + name + " PORT IN USE, INSPECT THE CONSTANTS.JAVA FILE! ***")
        return True
    else:
        print("\033[92mNo " + name + " port conflictions detected in the constants.java file :)\033[0m")
        f.write("\nNo " + name + " port conflictions detected in the constants.java file :)")
        return False

def checkForOverlaps():
    f.write("detected DIO's in use: \n")
    printPins(dios)
    f.write("\ndetected PWM's in use: \n")
    printPins(pwms)

    print("\n")
    error1 = evaluateForOverlaps(dios, "DIO")
    error2 = evaluateForOverlaps(pwms, "PWM")

    f.write("\n")
    print("\033[94mok\n\033[0m")
    f.close()

    if (error1 or error2):
        exit(-1)
    else:
        exit(0)


if __name__ == "__main__":
    printDelcaredTypes()
    for type_decl in tree.type_declarations:
        evaluateFieldDeclarations(type_decl)
    checkForOverlaps()
